;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'looking-through
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  org-files-in-order '("looking-through-codegen" "looking-through")
  site-lisp-config-prefix "50"
  license "GPL-3")

(advice-add 'fakemake-test :before
            (lambda () (require 'org-src-elisp-extras)))
